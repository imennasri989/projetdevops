import express from "express";
import {
    createFacture,
    getAllFactures,
    getFactureById,
    deleteFacture,
    updateFacture
} from "../controllers/factureController.js";

const router = express.Router();

// Routes for Facture
router.post("/createfacture", createFacture);
router.get("/getAll", getAllFactures);
router.get("/:id", getFactureById);
router.delete('/delete/:id', deleteFacture);
router.put("/updateFacture/:id", updateFacture);

export default router;
