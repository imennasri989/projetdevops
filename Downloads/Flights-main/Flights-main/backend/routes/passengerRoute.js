import express from "express";
import {
    createPassenger,
    getAllPassengers,
    getPassengerById,
    deletePassenger,
    updatePassenger,
    searchPassenger
} from "../controllers/passengerController.js";

//router object
const router = express.Router();

//routing
//CREATE Passenger || METHOD POST
router.post("/createpassenger", createPassenger);

//GET ALL  || GET
router.get("/getAll", getAllPassengers);

//GET  By Id || GET
router.get("/:id", getPassengerById);

//DELETE || DELETE
router.delete('/delete/:id', deletePassenger);

//update  || UPDATE
router.put("/updatePassenger/:id" , updatePassenger);

//SEARCH
router.get('/search/:key',searchPassenger);




export default router;
