import express from "express";
import {
    createFlight,
    getAllFlights,
    getFlightById,
    deleteFlight,
    updateFlight,
    searchFlight
} from "../controllers/FlightController.js";

const router = express.Router();

// Routes for Flight
router.post("/createflight", createFlight);
router.get("/getAll", getAllFlights);
router.get("/:id", getFlightById);
router.delete('/delete/:id', deleteFlight);
router.put("/updateFlight/:id", updateFlight);
router.get('/search/:key', searchFlight);

export default router;
