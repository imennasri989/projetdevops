import express from "express";
import {
    createHotel,
    getAllHotels,
    getHotelById,
    deleteHotel,
    updateHotel,
    searchHotel
} from "../controllers/hotelController.js";

//router object
const router = express.Router();

//routing
//CREATE HOTEL || METHOD POST
router.post("/createhotel", createHotel);

//GET ALL HOTElS || GET
router.get("/getAll", getAllHotels);

//GET HOTElS By Id || GET
router.get("/:id", getHotelById);

//DELETE HOTEL || DELETE
router.delete('/delete/:id', deleteHotel);

//update HOTEL || UPDATE
router.put("/updateHotel/:id" , updateHotel);

//SEARCH
router.get('/search/:key',searchHotel);




export default router;
