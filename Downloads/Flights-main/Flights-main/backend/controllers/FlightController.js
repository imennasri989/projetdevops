import Flight from "../models/FlightModel.js";
import asyncHandler from "express-async-handler";

// @Route
// Method POST http://localhost:8081/flight/createflight
export const createFlight = asyncHandler(async (req, res) => {
  const {  
    pricePurchase,
    priceSale,
    departureDate,
    entryDate,
    coupon,
    destination,
    journeyName
  } = req.body;

  if (!pricePurchase || !priceSale || !departureDate || !entryDate || !coupon || !destination || !journeyName) {
    res.json({ "message": "Please add all fields" }).status(402)
    throw new Error('Please add all fields');
  }
  
  const flight = await Flight.create({
    pricePurchase,
    priceSale,
    departureDate,
    entryDate,
    coupon,
    destination,
    journeyName
  });

  if (flight) {
    res.status(201).json({
      _id: flight.id,
      pricePurchase: flight.pricePurchase,
      priceSale: flight.priceSale,
      departureDate: flight.departureDate,
      entryDate: flight.entryDate,
      coupon: flight.coupon,
      destination: flight.destination,
      journeyName: flight.journeyName
    });
  } else {
    res.status(400);
    throw new Error('Invalid flight data');
  }
});

// @Route
// Method GET http://localhost:8081/flight/getAll
export const getAllFlights = asyncHandler(async (req, res) => {
  const flights = await Flight.find({});
  res.json(flights);
});

// @Route
// Method GET http://localhost:8081/flight/:id
export const getFlightById = asyncHandler(async (req, res) => {
  const flight = await Flight.findById(req.params.id);
  if (flight) {
    res.json(flight);
  } else {
    res.status(404);
    throw new Error('Flight not found');
  }
});

// @Route
// Method DELETE http://localhost:8081/flight/delete/:id
export const deleteFlight = asyncHandler(async (req, res) => {
  const flight = await Flight.findById(req.params.id);
  if (flight) {
    await flight.deleteOne(); // Use deleteOne or deleteMany based on your needs
    res.json("Flight removed" )
  } else {
    res.status(404)
    throw new Error('Flight not found')
  }
});

// @Route
// Method PUT http://localhost:8081/flight/updateFlight/:id
export const updateFlight = asyncHandler(async (req, res) => {
  console.log('req.file:', req.file);
  console.log('req.body:', req.body);

  const {
    pricePurchase,
    priceSale,
    departureDate,
    entryDate,
    coupon,
    destination,
    journeyName
  } = req.body;

  const flight = await Flight.findById(req.params.id);

  if (flight) {
    flight.pricePurchase = pricePurchase;
    flight.priceSale = priceSale;
    flight.departureDate = departureDate;
    flight.entryDate = entryDate;
    flight.coupon = coupon;
    flight.destination = destination;
    flight.journeyName = journeyName;

    const updatedFlight = await flight.save();

    if (updatedFlight) {
      res.status(200).json({
        _id: updatedFlight.id,
        pricePurchase: updatedFlight.pricePurchase,
        priceSale: updatedFlight.priceSale,
        departureDate: updatedFlight.departureDate,
        entryDate: updatedFlight.entryDate,
        coupon: updatedFlight.coupon,
        destination: updatedFlight.destination,
        journeyName: updatedFlight.journeyName
      });
    } else {
      res.status(500);
      throw new Error('Error updating flight');
    }
  } else {
    res.status(404);
    throw new Error('Flight not found');
  }
});

// @Route
// Method GET http://localhost:8081/flight/search/:key
export const searchFlight = asyncHandler( async (req, res) => {
  const key = req.params.key;
  
  const flightResults = await Flight.find({
    $or: [
      { destination: { $regex:  new RegExp(key, 'i')  } },
      { journeyName: { $regex:  new RegExp(key, 'i')  } },
    ],
  });

  const results = flightResults;
  
  res.send(results);
});
