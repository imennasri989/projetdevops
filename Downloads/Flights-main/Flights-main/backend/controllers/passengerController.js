import Passenger from "../models/passengerModel.js";
import asynHandler from "express-async-handler";


//@Route
// Method POST http://localhost:8081/passenger/createpassenger

export const createPassenger = asynHandler(async (req, res) => {
 
    const {  
        passengerName ,
        phone ,
        email
    } = req.body

    if (!passengerName || !phone|| !email) {
      res.json({"message":"Please add  all fields"}).status(402)
          throw new Error('Please add  all fields')
  }
   
  const passenger = await Passenger.create({
    passengerName ,
    phone ,
    email
  })
 
 if(passenger){
    res.status(201).json({
        _id: passenger.id,
        passengerName: passenger.passengerName,
        phone: passenger.phone,
        email: passenger.email
    })
}

else{
    res.status(400)
    throw new Error('Invalid user data')
}

});


//@Route
// Method GET http://localhost:8081/passenger/getAll
export const getAllPassengers = asynHandler(async(req,res)=>{
  
const passenger = await Passenger.find({});
if (!passenger) {
    res.Error(404)
    throw new Error(" Passenger Not Found !!")
}
res.json(passenger)

});

//@Route
// Method GET http://localhost:8081/passenger/:id

export const getPassengerById = asynHandler(async (req, res) => {
const passenger = await Passenger.findById(req.params.id)

if (passenger) {
  res.json(passenger)
} else {
  res.status(404)
  throw new Error('passenger not found')
}
});

//@Route
// Method DELETE http://localhost:8081/passenger/delete/:id

export const deletePassenger = asynHandler(async (req, res) => {
const passenger = await Passenger.findById(req.params.id)

if (passenger) {
    await passenger.deleteOne(); // Use deleteOne or deleteMany based on your needs
  res.json("passenger removed" )
} else {
  res.status(404)
  throw new Error('passenger not found')
}
});

// Method PUT http://localhost:8081/passenger/updatePassenger/:id

export const updatePassenger = asynHandler(async (req, res) => {
  console.log('req.file:', req.file);
  console.log('req.body:', req.body);

  const { passengerName, phone, email } = req.body;

  const passenger = await Passenger.findById(req.params.id);

  if (passenger) {
    passenger.passengerName = passengerName;
    passenger.email = email;
    passenger.phone = phone;

    const updatedPassenger = await passenger.save();

    if (updatedPassenger) {
      res.status(200).json({
        _id: updatedPassenger.id,
        passengerName: updatedPassenger.passengerName,
        email: updatedPassenger.email,
        phone: updatedPassenger.phone
      });
    } else {
      res.status(500);
      throw new Error('Error updating passenger');
    }
  } else {
    res.status(404);
    throw new Error('Passenger not found');
  }
});

// Method GET http://localhost:8081/passenger/search/:key
export const searchPassenger = asynHandler( async (req, res) => {
    const key = req.params.key;
    
    const passengerResults = await Passenger.find({
      $or: [
        { passengerName: { $regex:  new RegExp(key, 'i')  } },
        { email: { $regex:  new RegExp(key, 'i')  } },
        { phone: { $regex:  new RegExp(key, 'i')  } },
      ],
    });
  
    const results = passengerResults;
    
    res.send(results);
  });

