import Hotel from "../models/hotelModel.js";
import asynHandler from "express-async-handler";


//@Route
// Method POST http://localhost:8081/hotel/createhotel

export const createHotel = asynHandler(async (req, res) => {
 
    const {  
        hotelName ,
        cityName , 
        Adress , 
        phone ,
      user
    } = req.body

    if (!hotelName || !cityName|| !Adress || !phone) {
      res.json({"message":"Please add  all fields"}).status(402)
          throw new Error('Please add  all fields')
  }
   
  const hotel = await Hotel.create({
        hotelName ,
        cityName , 
        Adress , 
        phone ,
        user
  })
 
 if(hotel){
    res.status(201).json({
        _id: hotel.id,
        hotelName: hotel.hotelName,
        user:hotel.user,
        cityName: hotel.cityName,
        Adress: hotel.Adress,
        phone: hotel.phone
    })
}

else{
    res.status(400)
    throw new Error('Invalid user data')
}

});


//@Route
// Method GET http://localhost:8081/hotel/getAll
export const getAllHotels = asynHandler(async(req,res)=>{
  
const hotel = await Hotel.find( {}).populate('user');
if (!hotel) {
    res.Error(404)
    throw new Error(" Hotel Not Found !!")
}
res.json(hotel)

});

//@Route
// Method GET http://localhost:8081/hotel/:id
export const getHotelById = asynHandler(async (req, res) => {
const hotel = await Hotel.findById(req.params.id)

if (hotel) {
  res.json(hotel)
} else {
  res.status(404)
  throw new Error('Hotel not found')
}
});

export const GetHotelsById = asynHandler(  async (req, res) => {
try {
  const hotel = await Hotel.find( { user: req.params.userId } ).populate('user'); 
  if (!hotel) {
    return res.status(404).json({ message: 'hotel not found' });
  }
  res.json(hotel);
} catch (error) {
  console.error(error);
  res.status(500).json({ message: 'Server error' });
}
});

//@Route
// Method DELETE http://localhost:8081/hotel/delete/:id
export const deleteHotel = asynHandler(async (req, res) => {
const hotel = await Hotel.findById(req.params.id)

if (hotel) {
    await hotel.deleteOne(); // Use deleteOne or deleteMany based on your needs
  res.json("hotel removed" )
} else {
  res.status(404)
  throw new Error('hotel not found')
}
});

// Method PUT http://localhost:8081/hotel/updateHotel/:id
export const updateHotel = asynHandler(async (req, res) => {

console.log('req.file:', req.file);
console.log('req.body:', req.body);
const {
    hotelName ,
    cityName , 
    Adress , 
    phone ,
} = req.body

const hotel = await Hotel.findById(req.params.id)

if (hotel) {
    hotel.hotelName = hotelName
    hotel.cityName = cityName
    hotel.Adress = Adress
    hotel.phone = phone

  const updatedHotel = await hotel.save()
  if (updatedHotel){
    res.status(201).json({
      _id: hotel.id,
      hotelName: hotel.hotelName,
      user : hotel.user,
      cityName: hotel.cityName,
      Adress: hotel.Adress,
      phone: hotel.phone
    })
  }
} else {
  res.status(404)
  throw new Error('Hotel not found')
}
});

// Method GET http://localhost:8081/hotel/search/:key
export const searchHotel = asynHandler( async (req, res) => {
    const key = req.params.key;
    
    const hotelResults = await Hotel.find({
      $or: [
        { hotelName: { $regex:  new RegExp(key, 'i')  } },
        { cityName: { $regex:  new RegExp(key, 'i')  } },
        { Adress: { $regex:  new RegExp(key, 'i')  } },
        { phone: { $regex:  new RegExp(key, 'i')  } },
      ],
    });
  
    const results = hotelResults;
    
    res.send(results);
  });

