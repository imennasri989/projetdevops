import Facture from "../models/factureModel.js";
import asyncHandler from "express-async-handler";

// @Route
// Method POST http://localhost:8081/facture/createfacture
export const createFacture = asyncHandler(async (req, res) => {
  const {  
    clientName,
    totalPrice,
    profit
  } = req.body;

  if (!clientName || !totalPrice || !profit) {
    res.status(400).json({ message: "Please provide all required fields" });
    throw new Error('Please provide all required fields');
  }
  
  const facture = await Facture.create({
    clientName,
    totalPrice,
    profit
  });

  if (facture) {
    res.status(201).json({
      _id: facture.id,
      clientName: facture.clientName,
      totalPrice: facture.totalPrice,
      profit: facture.profit
    });
  } else {
    res.status(400);
    throw new Error('Invalid facture data');
  }
});

// @Route
// Method GET http://localhost:8081/facture/getAll
export const getAllFactures = asyncHandler(async (req, res) => {
  const factures = await Facture.find({});
  res.json(factures);
});

// @Route
// Method GET http://localhost:8081/facture/:id
export const getFactureById = asyncHandler(async (req, res) => {
  const facture = await Facture.findById(req.params.id);
  if (facture) {
    res.json(facture);
  } else {
    res.status(404).json({ message: 'Facture not found' });
  }
});

// @Route
// Method DELETE http://localhost:8081/facture/delete/:id
export const deleteFacture = asyncHandler(async (req, res) => {
  const facture = await Facture.findById(req.params.id);
  if (facture) {
    await facture.remove();
    res.json({ message: 'Facture removed' });
  } else {
    res.status(404).json({ message: 'Facture not found' });
  }
});

// @Route
// Method PUT http://localhost:8081/facture/updateFacture/:id
export const updateFacture = asyncHandler(async (req, res) => {
  const { clientName, totalPrice, profit } = req.body;

  const facture = await Facture.findById(req.params.id);

  if (facture) {
    facture.clientName = clientName || facture.clientName;
    facture.totalPrice = totalPrice || facture.totalPrice;
    facture.profit = profit || facture.profit;

    const updatedFacture = await facture.save();
    res.json(updatedFacture);
  } else {
    res.status(404).json({ message: 'Facture not found' });
  }
});
