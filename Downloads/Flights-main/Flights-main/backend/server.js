import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import connectDB from './config/db.js';
import authRoutes from './routes/authRoute.js';
import hotelRoutes from './routes/hotelRoute.js';
import passengerRoutes from './routes/passengerRoute.js';
import factureRoutes from './routes/FactureRoute.js'; // Ajout de cette ligne
import flightRoutes from './routes/FlightRoute.js'; // Ajout de cette ligne

import cors from 'cors';

dotenv.config();
connectDB();

const app = express();

// Enable CORS
app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true, // If you're using cookies or other credentials
}));

// Middleware
app.use(express.json());
app.use(morgan('dev'));

// Routes
app.use('/api/v1/auth', authRoutes);
app.use('/hotel', hotelRoutes);
app.use('/passenger', passengerRoutes);
app.use('/facture', factureRoutes); // Ajout de cette ligne
app.use('/flight', flightRoutes); // Ajout de cette ligne

// Default route
app.get('/', (req, res) => {
    res.send('<h1>Welcome to the E-commerce App</h1>');
});

// Port
const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
    console.log(`Server running in ${process.env.DEV_MODE} mode on port ${PORT}`);
});
