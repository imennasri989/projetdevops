import mongoose from "mongoose";

const hotelSchema = new mongoose.Schema(
  {
    user : {type: mongoose.Types.ObjectId,ref:'users'},
    
    hotelName: {
      type: String,
      required: true,
      trim: true,
    },
    cityName: {
        type: String,
        required: true,
        trim: true,
      },
      Adress: {
        type: String,
        required: true,
        trim: true,
      },
      phone: {
        type: String,
        required: true,
        trim: true,
      }
  },
  { timestamps: true }
);

export default mongoose.model("Hotel", hotelSchema);