import mongoose from "mongoose";

const passengerSchema = new mongoose.Schema(
  {

    passengerName: {
      type: String,
      required: true,
      trim: true,
    },
      phone: {
        type: String,
        required: true,
        trim: true,
      },
      email: {
        type: String,
        required: true,
        trim: true,
      }
      
  },
  { timestamps: true }
);

export default mongoose.model("Passenger", passengerSchema);