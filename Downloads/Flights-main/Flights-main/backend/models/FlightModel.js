import mongoose from "mongoose";

const flightSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Types.ObjectId, ref: 'users' },
    pricePurchase: {
      type: Number,
      required: true,
    },
    priceSale: {
      type: Number,
      required: true,
    },
    departureDate: {
      type: Date,
      required: true,
    },
    entryDate: {
      type: Date,
      required: true,
    },
    coupon: {
      type: String,
      required: true,
      trim: true,
    },
    destination: {
      type: String,
      required: true,
      trim: true,
    },
    // Assuming "nom voyage" refers to the name of the journey
    journeyName: {
      type: String,
      required: true,
      trim: true,
    }
  },
  { timestamps: true }
);

export default mongoose.model("Flight", flightSchema);
